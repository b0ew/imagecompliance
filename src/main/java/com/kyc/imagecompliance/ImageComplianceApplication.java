package com.kyc.imagecompliance;

import org.opencv.core.Core;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImageComplianceApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ImageComplianceApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }
}
