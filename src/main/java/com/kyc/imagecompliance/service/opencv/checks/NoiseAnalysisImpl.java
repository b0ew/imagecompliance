package com.kyc.imagecompliance.service.opencv.checks;

import com.kyc.imagecompliance.service.contracts.conditions.NoiseAnalysis;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class NoiseAnalysisImpl implements NoiseAnalysis {
    @Override
    public boolean isCompliant(Mat image) {
       double noiseAmount = calculateNoisePixelIntensity(image);
       log.info("Noise amount is " + noiseAmount);
       return noiseAmount <=100;
    }

    @Override
    public double calculateNoisePixelIntensity(Mat image) {


        // Apply Gaussian blur

        // Calculate the standard deviation of pixel intensities
        MatOfDouble mean = new MatOfDouble();
        MatOfDouble stddev = new MatOfDouble();
        Core.meanStdDev(image, mean, stddev);

        return stddev.get(0,0)[0];
    }
}
