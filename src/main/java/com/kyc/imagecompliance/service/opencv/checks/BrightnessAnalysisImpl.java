package com.kyc.imagecompliance.service.opencv.checks;

import com.kyc.imagecompliance.service.contracts.conditions.BrightnessAnalysis;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Mat;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BrightnessAnalysisImpl implements BrightnessAnalysis {
    private static final int brightnessLowerThreshold = 50;
    private static final int brightnessUpperThreshold = 230;

    @Override
    public boolean isCompliant(Mat image) {
        double imageBrightness = calculateBrightness(image);
        log.info("Brightness of the image is: " + imageBrightness);
        return !(imageBrightness <= 33) && !(imageBrightness > 94);
    }

    public double calculateBrightness(Mat image) {
     // Hue Saturation Value(brightness)
        Mat hsv = new Mat();
        Imgproc.cvtColor(image, hsv, Imgproc.COLOR_BGR2HSV);

        double sum = 0;
        for (int i = 0; i < hsv.rows(); i++) {
            for (int j = 0; j < hsv.cols(); j++) {
                sum += hsv.get(i, j)[2];
            }
        }
        double avg = sum / (hsv.rows() * hsv.cols());
        return (avg / 255) * 100;
    }
}
