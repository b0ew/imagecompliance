package com.kyc.imagecompliance.service.opencv.checks;

import com.kyc.imagecompliance.service.contracts.conditions.SharpnessAndBlurinessAnalysis;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SharpnessAndBlurinessAnalysisImpl implements SharpnessAndBlurinessAnalysis {

    public static final int LAPLACIAN_LOWER_THRESHOLD = 80;
    public static final int LAPLACIAN_UPPER_THRESHOLD = 150;


    @Override
    public boolean isCompliant(Mat image) {
        double laplacianComputationOfImage = computeBluriness(image);
        log.info("Sharpness and bluriness is  " + laplacianComputationOfImage);
        return !isBlurryAsPerGivenThreshold(LAPLACIAN_LOWER_THRESHOLD,LAPLACIAN_UPPER_THRESHOLD ,laplacianComputationOfImage);
    }


    @Override
    public double computeBluriness(Mat image) {
        Imgproc.GaussianBlur(image, image, new Size(3, 3), 0, 0, Core.BORDER_DEFAULT);

        // Convert the image to grayscale
        Mat imageGray = new Mat();
        Imgproc.cvtColor(image, imageGray, Imgproc.COLOR_RGB2GRAY);

        // Apply Laplacian operator
        Mat laplacian = new Mat();
        Imgproc.Laplacian(imageGray, laplacian, CvType.CV_64F);

        // Calculate the variance of the Laplacian
        MatOfDouble mean = new MatOfDouble();
        MatOfDouble stdDev = new MatOfDouble();
        Core.meanStdDev(laplacian, mean, stdDev);

        double variance = Math.pow(stdDev.get(0, 0)[0], 2);
        log.info("Variance is " + variance);

        return variance;
    }

    @Override
    public boolean isBlurryAsPerGivenThreshold(double laplacianLowerThreshold,double laplacianUpperThreshold ,double laplacianComputationOfImage) {
        return laplacianComputationOfImage < laplacianLowerThreshold || laplacianComputationOfImage > laplacianUpperThreshold;
    }
}
