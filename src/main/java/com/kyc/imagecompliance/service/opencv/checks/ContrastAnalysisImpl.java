package com.kyc.imagecompliance.service.opencv.checks;

import com.kyc.imagecompliance.service.contracts.conditions.ContrastAnalysis;
import org.opencv.core.Mat;
import org.springframework.stereotype.Component;

@Component
public class ContrastAnalysisImpl implements ContrastAnalysis {
    @Override
    public boolean isCompliant(Mat image) {
        return true;
    }
}
