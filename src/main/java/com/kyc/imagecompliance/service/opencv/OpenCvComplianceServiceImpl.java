package com.kyc.imagecompliance.service.opencv;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.opencv.OpenCvInput;
import com.kyc.imagecompliance.api.opencv.OpenCvResult;
import com.kyc.imagecompliance.service.contracts.conditions.BrightnessAnalysis;
import com.kyc.imagecompliance.service.contracts.conditions.ContrastAnalysis;
import com.kyc.imagecompliance.service.contracts.conditions.NoiseAnalysis;
import com.kyc.imagecompliance.service.contracts.conditions.SharpnessAndBlurinessAnalysis;
import com.kyc.imagecompliance.service.contracts.services.OpenCvComplianceService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
@RequiredArgsConstructor
@Slf4j
public class OpenCvComplianceServiceImpl implements OpenCvComplianceService {
    private final ConversionService conversionService;
    private final BrightnessAnalysis brightnessAnalysis;
    private final ContrastAnalysis contrastAnalysis;
    private final NoiseAnalysis noiseAnalysis;
    private final SharpnessAndBlurinessAnalysis sharpnessAndBlurinessAnalysis;

    @Override
    @SneakyThrows
    public OpenCvResult checkComplianceOpenCv(OpenCvInput openCvInput) {
        Mat convertedImage = convertMultiPartToMat(openCvInput.getMultipartFile());
        boolean isBrightCompliant = brightnessAnalysis.isCompliant(convertedImage);
        if (!isBrightCompliant) {
            return conversionService.convert(ImageCompliance.NON_COMPLIANT, OpenCvResult.class);
        }
        boolean isSharpAndBlurryCompliant = sharpnessAndBlurinessAnalysis.isCompliant(convertedImage);
        boolean isNoiseCompliant = noiseAnalysis.isCompliant(convertedImage);
        if (!isNoiseCompliant) {
            return conversionService.convert(ImageCompliance.NON_COMPLIANT, OpenCvResult.class);
        }
        return isSharpAndBlurryCompliant
                ? conversionService.convert(ImageCompliance.COMPLIANT, OpenCvResult.class)
                : conversionService.convert(ImageCompliance.NON_COMPLIANT, OpenCvResult.class);
    }

    @SneakyThrows
    public Mat convertMultiPartToMat(MultipartFile multipartFile) {
        byte[] fileToByteArray = multipartFile.getBytes();
        MatOfByte matOfByte = new MatOfByte(fileToByteArray);
        return Imgcodecs.imdecode(matOfByte, Imgcodecs.IMREAD_UNCHANGED);
    }
}
