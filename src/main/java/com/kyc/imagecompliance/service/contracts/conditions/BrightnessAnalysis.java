package com.kyc.imagecompliance.service.contracts.conditions;

import com.kyc.imagecompliance.service.contracts.conditions.generic.GenericConditionAnalysis;
import org.opencv.core.Mat;

public interface BrightnessAnalysis extends GenericConditionAnalysis {
    double calculateBrightness(Mat image);
}
