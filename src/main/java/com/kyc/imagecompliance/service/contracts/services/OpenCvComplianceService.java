package com.kyc.imagecompliance.service.contracts.services;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.opencv.OpenCvInput;
import com.kyc.imagecompliance.api.opencv.OpenCvResult;

public interface OpenCvComplianceService {
    OpenCvResult checkComplianceOpenCv(OpenCvInput openCvInput);
}
