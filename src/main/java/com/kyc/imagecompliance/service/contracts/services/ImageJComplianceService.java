package com.kyc.imagecompliance.service.contracts.services;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.imageJ.ImagejInput;
import com.kyc.imagecompliance.api.imageJ.ImagejResult;
import com.kyc.imagecompliance.api.opencv.OpenCvInput;

public interface ImageJComplianceService {


    ImagejResult checkComplianceImageJ(ImagejInput imagejInput);
}
