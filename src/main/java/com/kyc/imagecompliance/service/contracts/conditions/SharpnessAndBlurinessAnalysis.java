package com.kyc.imagecompliance.service.contracts.conditions;

import com.kyc.imagecompliance.service.contracts.conditions.generic.GenericConditionAnalysis;
import org.opencv.core.Mat;

public interface SharpnessAndBlurinessAnalysis extends GenericConditionAnalysis {
    double computeBluriness(Mat image);
    boolean isBlurryAsPerGivenThreshold(double laplacianLowerThreshold,double laplacianUpperThreshold ,double laplacianComputationOfImage);
}
