package com.kyc.imagecompliance.service.contracts.conditions.generic;

import org.opencv.core.Mat;

public interface GenericConditionAnalysis {
    boolean isCompliant(Mat image);
}
