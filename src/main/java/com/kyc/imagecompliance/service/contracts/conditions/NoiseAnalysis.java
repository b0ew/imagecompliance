package com.kyc.imagecompliance.service.contracts.conditions;

import com.kyc.imagecompliance.service.contracts.conditions.generic.GenericConditionAnalysis;
import org.opencv.core.Mat;

public interface NoiseAnalysis extends GenericConditionAnalysis {
    double calculateNoisePixelIntensity(Mat image);
}
