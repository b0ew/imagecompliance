package com.kyc.imagecompliance.service.contracts.conditions;

import com.kyc.imagecompliance.service.contracts.conditions.generic.GenericConditionAnalysis;

public interface ContrastAnalysis extends GenericConditionAnalysis {
}
