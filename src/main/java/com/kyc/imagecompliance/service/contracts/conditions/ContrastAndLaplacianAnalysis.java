package com.kyc.imagecompliance.service.contracts.conditions;

import com.kyc.imagecompliance.service.contracts.conditions.generic.GenericConditionAnalysis;
import org.opencv.core.Mat;

public interface ContrastAndLaplacianAnalysis extends GenericConditionAnalysis {
    double measureContrast(Mat image);

    double measureSharpness(Mat image);
}
