package com.kyc.imagecompliance.service.imageJ;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.imageJ.ImagejInput;
import com.kyc.imagecompliance.api.imageJ.ImagejResult;
import com.kyc.imagecompliance.service.contracts.services.ImageJComplianceService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ImageJComplianceServiceImpl implements ImageJComplianceService {
private final ConversionService conversionService;

    @Override
    public ImagejResult checkComplianceImageJ(ImagejInput imagejInput) {
        return conversionService.convert(ImageCompliance.COMPLIANT, ImagejResult.class);
    }
}
