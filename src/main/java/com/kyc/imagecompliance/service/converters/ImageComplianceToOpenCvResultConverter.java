package com.kyc.imagecompliance.service.converters;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.opencv.OpenCvResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ImageComplianceToOpenCvResultConverter implements Converter<ImageCompliance, OpenCvResult> {
    @Override
    public OpenCvResult convert(ImageCompliance imageCompliance) {
        return (OpenCvResult) OpenCvResult.builder()
                .imageCompliance(imageCompliance)
                .build();
    }
}
