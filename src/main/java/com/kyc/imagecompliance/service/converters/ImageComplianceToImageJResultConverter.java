package com.kyc.imagecompliance.service.converters;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import com.kyc.imagecompliance.api.imageJ.ImagejResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class ImageComplianceToImageJResultConverter implements Converter<ImageCompliance, ImagejResult> {
    @Override
    public ImagejResult convert(ImageCompliance imageCompliance) {
        return (ImagejResult) ImagejResult.builder()
                .imageCompliance(imageCompliance)
                .build();
    }
}
