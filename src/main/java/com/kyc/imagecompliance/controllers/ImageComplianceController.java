package com.kyc.imagecompliance.controllers;

import com.kyc.imagecompliance.api.imageJ.ImagejInput;
import com.kyc.imagecompliance.api.imageJ.ImagejResult;
import com.kyc.imagecompliance.api.opencv.OpenCvInput;
import com.kyc.imagecompliance.api.opencv.OpenCvResult;
import com.kyc.imagecompliance.service.contracts.services.ImageJComplianceService;
import com.kyc.imagecompliance.service.contracts.services.OpenCvComplianceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/image-compliance")
@RequiredArgsConstructor
public class ImageComplianceController {

    private final ImageJComplianceService imageJComplianceService;
    private final OpenCvComplianceService openCvComplianceService;

    @GetMapping(value = "/open-cv", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<OpenCvResult> checkComplianceOpenCv(@RequestPart("file") MultipartFile multipartFile) {
        OpenCvInput openCvInput = (OpenCvInput) OpenCvInput.builder()
                .multipartFile(multipartFile)
                .build();
        return ResponseEntity.ok(openCvComplianceService.checkComplianceOpenCv(openCvInput));
    }

    @GetMapping(value = "/imagej", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ImagejResult> checkComplianceImageJ(@RequestPart("file") MultipartFile multipartFile) {
        ImagejInput imagejInput = (ImagejInput) ImagejInput.builder()
                .multipartFile(multipartFile)
                .build();
        return ResponseEntity.ok(imageJComplianceService.checkComplianceImageJ(imagejInput));
    }


}
