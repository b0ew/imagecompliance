package com.kyc.imagecompliance.api.enums;

public enum ImageCompliance {
    COMPLIANT,
    NON_COMPLIANT
}
