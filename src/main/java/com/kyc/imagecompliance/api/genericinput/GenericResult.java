package com.kyc.imagecompliance.api.genericinput;

import com.kyc.imagecompliance.api.enums.ImageCompliance;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@AllArgsConstructor
@SuperBuilder
public class GenericResult {
    private ImageCompliance imageCompliance;
}
