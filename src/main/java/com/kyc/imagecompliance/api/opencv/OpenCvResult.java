package com.kyc.imagecompliance.api.opencv;

import com.kyc.imagecompliance.api.genericinput.GenericResult;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@SuperBuilder
public class OpenCvResult extends GenericResult {

}
