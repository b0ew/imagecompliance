package com.kyc.imagecompliance.api.imageJ;

import com.kyc.imagecompliance.api.genericinput.GenericInput;
import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter(value = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@SuperBuilder
public class ImagejInput extends GenericInput {

}
